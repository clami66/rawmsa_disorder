from sys import argv, exit
import glob
import re
import os

import numpy as np
from numpy import newaxis
#np.set_printoptions(threshold=np.nan)

from keras.models import Model, load_model
from keras.layers import Input, average, maximum
from keras import backend as K


def ensembleModels(models, model_inputs):


    yModels = [model(model_input) for (model, model_input) in zip(models, model_inputs)]

    yAvg = average(yModels)

    modelEns = Model(inputs=model_inputs, outputs=yAvg, name='ensemble')

    return modelEns


def true_positives(y_true, y_pred):


    correct_preds = K.cast(K.equal(K.flatten(y_true),  K.cast(K.argmax(y_pred, axis=-1), K.floatx())), K.floatx())

    true_pos = K.cast(K.sum(correct_preds * K.flatten(y_true)), 'int64')

    return true_pos


def true_negatives(y_true, y_pred):


    correct_preds = K.cast(K.equal(K.flatten(y_true),  K.cast(K.argmax(y_pred, axis=-1), K.floatx())), K.floatx())

    true_neg = K.cast(K.sum(correct_preds * (1 - K.flatten(y_true))), 'int64')

    return true_neg


def positives(y_true, y_pred):

    pos = K.cast(K.sum(K.flatten(y_true)), 'int64')

    return pos


def negatives(y_true, y_pred):

    neg = K.cast(K.sum(1 - K.flatten(y_true)), 'int64')

    return neg

input = argv[1]
output = os.path.splitext(input)[0] + ".pred"

input_aln_file = open(input)
output_file = open(output, "w")

depth = 0
length = 0
alignment_max_depth = 3000

for l in input_aln_file:

    if depth == 0:
        line_split = l.split()
        length = len(line_split)
        msa = np.zeros((length, alignment_max_depth))

    if depth < alignment_max_depth:
        msa[0:length, depth] = [int(element) for element in l.split()]
        depth += 1

all_models = glob.glob('models_disorder/*depth*[0-9].h5')
model_depths = [int(re.search("depth_(\d+)", model_path).group(1)) for model_path in all_models]

models = []
counter = 0

for (model, depth) in zip(all_models, model_depths):

    modelTemp = load_model(model, custom_objects={'true_positives':true_positives, 'positives':positives, 'newaxis': newaxis, 'true_negatives': true_negatives, 'negatives': negatives})  # load model
    modelTemp.summary()
    modelTemp.name = "model_" + str(counter)
    counter += 1

    models.append(modelTemp)

model_inputs = [Input(shape=model.input_shape[1:]) for model in models]

modelEns_thisfold = ensembleModels(models, model_inputs)
#modelEns_thisfold.summary()

X = []

for alignment_max_depth in model_depths:

    X.append(np.asarray(msa[:, :alignment_max_depth].reshape(length * alignment_max_depth)[newaxis, :]))

res = modelEns_thisfold.predict([x for x in X])
res = np.squeeze(res)

[output_file.write('{}\t{}\n'.format(i + 1, pred)) for (i, pred) in zip(range(res.shape[0]), res[:,1]-res[:,0]+0.5)]

output_file.close()
