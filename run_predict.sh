#!/bin/bash

HERE=`pwd`
HHBLITS=$HERE/hh-suite/build/bin/hhblits
HHDB=$HERE/hh-suite/databases/uniclust30_2017_10

SCRIPTS=$HERE/scripts
OUTPUTDIR=$HERE/outputs

export CUDA_VISIBLE_DEVICES="" #change if want to use GPU

if [[ $# -eq 0 ]] ; then

    echo $'\n\n###                     rawMSA disorder, a prototype.                      ###'
    echo $'### For more info: https://www.biorxiv.org/content/early/2018/08/17/394437 ###\n\n'

    echo 'Usage: ./run_predict.sh input.fasta'
    echo 'Help: ./run_predict.sh -h'
    echo $'Test installation: ./run_predict.sh -test\n'
    exit 0
elif [[ $1 == "-h" ]]; then
    cat README
    exit 0
elif [[ $1 == "-test" ]]; then

    echo $'\nTesting installation...'

    ./run_predict.sh test/example.fasta
    wait

    alignmentdiff=`diff test/example.a3m outputs/example.a3m`

    if [ ! -z "$alignmentdiff" ]; then
        echo "Something is wrong with the HHblits installation. Check outputs/example.hhlog for more deteils"
        exit 1
    fi

    convertdiff=`diff test/example.num outputs/example.num`

    if [ ! -z "$convertdiff" ]; then
        echo "Something is wrong with the alignment conversion scripts. Contact us for troubleshooting"
        exit 1
    fi

    predictdiff=`diff test/example.pred outputs/example.pred`

    if [ ! -z "$predictdiff" ]; then
        echo "Something is wrong with the prediction scripts. Check outputs/example.predlog for more details"
        exit 1
    fi

   echo "All good!"
   exit 0
fi

inputfasta=$1
id=`basename $inputfasta .fasta`

echo $'\n\n###                     rawMSA disorder, a prototype.                      ###'
echo $'### For more info: https://www.biorxiv.org/content/early/2018/08/17/394437 ###\n\n'

#Run alignments with hhblits
echo "Generating alignments, logs will be written to $OUTPUTDIR/$id.hhlog"
$HHBLITS -diff inf -cov 50 -id 99 -cpu 4 -i $inputfasta -d $HHDB -oa3m $OUTPUTDIR/$id.a3m -n 3 &> $OUTPUTDIR/$id.hhlog
egrep -v "^>" $OUTPUTDIR/$id.a3m | sed 's/[a-z]//g'   > $OUTPUTDIR/$id.aln

#convert flat fasta file to input for network
python3 $SCRIPTS/letters_to_numbers_3000.py $OUTPUTDIR/$id.aln > $OUTPUTDIR/$id.num

echo "Predicting disorder, output will be written to $OUTPUTDIR/$id.pred"
#prediction
python3 $SCRIPTS/predict_disorder.py $OUTPUTDIR/$id.num &> $OUTPUTDIR/$id.predlog
